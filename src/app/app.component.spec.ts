import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

import { DataTableModule } from 'angular-6-datatable';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { ExtrapageComponent } from './extrapage/extrapage.component';
import { DataTableComponent } from './data-table/data-table.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        DataTableModule
      ],
      declarations: [
        AppComponent,
        ExtrapageComponent,
        DataTableComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Capco'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Capco');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Capco!');
  });


///
/* describe(':', () => {

  // Begin by putting re-usable, preparatory code in a setup function instead of beforeEach().
  // The setup() function returns an object literal with the variables, such as app, that a test might reference.
  // You don't define semi-global variables (e.g., let app,fixture ) in the body of the describe().
  // Then each test invokes setup() in its first line, before continuing with steps that
  // manipulate the test subject and assert expectations.

  function setup() {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    return { fixture, app };
  }

  it('should create the app', async(() => {
    const { app } = setup();
    expect(app).toBeTruthy();
  }));

  it('should have title as \'Capco\'', async(() => {
    const { app } = setup();
    expect(app.title).toBe('Capco');
  }));

  it('should have h1 tag as \'Welcome to Capco!\'', async(() => {
    const { app, fixture } = setup();
    fixture.detectChanges();
    const compile = fixture.debugElement.nativeElement;
    const h1tag = compile.querySelector('h1');
    expect(h1tag.textContent).toBe('Welcome to Capco!');
  }));
}); */
///


});
