export class dataRequest{
    address_1: string;
    city: string;
    company: string;
    date_entry: string;
    date_exit: string;
    date_first: string;
    date_recent: string;
    email: string;
    fee: string;
    geo: string;
    guid: string;
    id: number;
    name: string;
    org_num: string;
    pan: string;
    phone: string;
    pin: string;
    status: string;
    url: string;
    zip: string;
}