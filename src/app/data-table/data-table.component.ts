

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import {DataServiceService} from '../data-service.service';
import {dataRequest} from '../dataRequest';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  //constructor(private httpService: HttpClient){
    constructor(private dataService: DataServiceService){

  }

 data: any;
  ngOnInit () {
    this.getData();
  }

 
  onClickMe(e:dataRequest) {
    
    //console.log(e.id + ' ' + e.status);
    //console.log("this.someService.postService(e).subscribe();");
    this.dataService.postData(e);
  }
  getData(): void{
    const success = response => {
      this.data = response;
      //console.log(response);
    }
    const failure = response => {
      //console.log(response);
    }

    this.dataService.getData()
        .subscribe(success, failure);
        //data => this.data = data
  }

}
