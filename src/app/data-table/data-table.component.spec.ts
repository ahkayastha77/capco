import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DataTableModule } from 'angular-6-datatable';
import { DataTableComponent } from './data-table.component';
import { HttpClientModule } from '@angular/common/http';

describe('DataTableComponent', () => {
  let component: DataTableComponent;
  let fixture: ComponentFixture<DataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataTableComponent ],
      imports : [DataTableModule,HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
