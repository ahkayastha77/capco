import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  url: string;

  constructor(private httpService: HttpClient) { }
  
  getData(): Observable<any> {
    return this.httpService.get('../assets/sample_data.json');
    }

  postData(e:any) : void{
     this.url = "https://jsonplaceholder.typicode.com/posts";
     console.log(this.url);
     console.log(e);

     //Below post method will call below post method....
     //this.httpService.post(this.url, e);
    }

  }

  


    
  

