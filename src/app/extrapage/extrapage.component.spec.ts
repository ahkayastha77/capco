import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtrapageComponent } from './extrapage.component';
import { HttpClientModule } from '@angular/common/http';

describe('ExtrapageComponent', () => {
  let component: ExtrapageComponent;
  let fixture: ComponentFixture<ExtrapageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtrapageComponent ],
      imports : [HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtrapageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
