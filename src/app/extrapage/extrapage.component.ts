import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-extrapage',
  templateUrl: './extrapage.component.html',
  styleUrls: ['./extrapage.component.css']
})
export class ExtrapageComponent implements OnInit {

  constructor(private httpService: HttpClient){

  }
  arrSample : string[];

  ngOnInit () {
    this.httpService.get('./assets/sample_data.json').subscribe(
      data => {
        this.arrSample = data as string [];	 // FILL THE ARRAY WITH DATA.
        //  console.log(this.arrBirds[1]);
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

}
